const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackWatchedGlobEntries = require('webpack-watched-glob-entries-plugin');
const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require("path");
const webpack = require('webpack');


function sassRules() {
    return [
        {
            test: /\.(sass|scss|css)$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        path: path.join(__dirname, "/public/css/")
                    },
                },
                {
                    loader: "css-loader",
                },
                {
                    loader: "sass-loader",
                }
            ]
        }
    ]
}


module.exports = {
    entry: WebpackWatchedGlobEntries.getEntries(
        [
            path.resolve(__dirname, './resources/assets/scripts/**/*.js')
        ],
        {
            ignore: '**/*.test.js'
        }
    ),
    output: {
        filename: 'js/[name].js',
        chunkFilename: 'js/chunks/[name].js',
        path: path.join(__dirname, "/public/"),
    },
    module: {
        rules: sassRules()
    },
    watch: true,
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css",
        }),
        new MergeIntoSingleFilePlugin({
            files:
            {
                "vendors/css/bootswatch.min.css": [
                    path.resolve(__dirname, "node_modules/bootswatch/dist/darkly/bootstrap.min.css")
                ],
                "vendors/js/jquery.min.js": [
                    path.resolve(__dirname, "node_modules/jquery/dist/jquery.min.js")
                ],
                "vendors/js/bootstrap.min.js": [
                    path.resolve(__dirname, "node_modules/bootstrap/dist/js/bootstrap.min.js")
                ],
            }
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            jquery: 'jquery'
        })
    ],
}



