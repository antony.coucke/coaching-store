'use strict'

class ProductController {
    get({view}) {
        return view.render('product/get')
    }
}

module.exports = ProductController
